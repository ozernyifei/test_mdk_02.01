﻿using BuilderApp.DB;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BuilderApp.AppWindows
{
    /// <summary>
    /// Логика взаимодействия для OrderWindow.xaml
    /// </summary>
    public partial class OrderWindow : Window
    {
        public int DeliveryFee = 0;

        public bool isPromoActivated = false;

        public OrderWindow()
        {
            InitializeComponent();
            SetListValues();
            CmbDeliveryType.ItemsSource = Helper.EF.Context.DeliveryType.ToList();
            CmbDeliveryType.DisplayMemberPath = "Title";
        }

       

        public void SetListValues()
        {
            ObservableCollection<DB.VW_ProductList> listCart = new ObservableCollection<DB.VW_ProductList>(Helper.OrderCart.orderCart);
            LVMiniProductList.ItemsSource = listCart.Distinct();
            decimal totalSum = 0;
            foreach (var item in listCart)
            {
                totalSum += item.Price * item.Quantity;
            }
            
            if (isPromoActivated) 
            {
                totalSum *= 0.1m;
            }

            TbTotalSum.Text = totalSum.ToString() + " ₽";
            
        }

        private void BtnAddItemToOrder_Click(object sender, RoutedEventArgs e)
        {
            ProductListWindow productList = new ProductListWindow();
            productList.Show();
            this.Close();
        }

        private void BtnBack_Click(object sender, RoutedEventArgs e)
        {
            MainWindow main = new MainWindow();
            if (Helper.OrderCart.orderCart.Count() != 0)
            {
                switch (MessageBox.Show("Вы уверены, что хотите закрыть заказ? Имеющийся заказ будет удален", "Закрыть окно", MessageBoxButton.YesNo, MessageBoxImage.Exclamation))
                {
                    case MessageBoxResult.Yes:
                        Helper.OrderCart.orderCart.Clear();
                        main.Show();
                        this.Close();
                        break;
                    case MessageBoxResult.No:
                        break;
                    default:
                        main.Show();
                        this.Close();
                        break;
                }
            }
            else
            {
                main.Show();
                this.Close();
            }
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            if (Helper.OrderCart.orderCart.Count() > 0)
            {
                if (TbDiscount.Text.Any(Char.IsLetter) || Convert.ToInt32(TbDiscount.Text) < 0 || Convert.ToInt32(TbDiscount.Text) > 100)
                {
                    MessageBox.Show("Значение скидки неподходит", "Ошибка значения", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                DB.Sale sale = new DB.Sale();

                sale.StaffId = Helper.AuthorizedUser.SavedEmployee.Id;
                sale.SaleDate = DateTime.Now;
                sale.Discount = Convert.ToDecimal(TbDiscount.Text) / 100;
                sale.DeliveryId = (CmbDeliveryType.SelectedItem as DB.DeliveryType).Id;

                Helper.EF.Context.Sale.Add(sale);
                Helper.EF.Context.SaveChanges();

                foreach (var item in Helper.OrderCart.orderCart.Distinct())
                {
                    ProductSale productSale = new ProductSale();
                    productSale.SaleId = sale.Id;
                    productSale.ProductId = item.Id;
                    productSale.Quantity = item.Quantity;

                    Helper.EF.Context.ProductSale.Add(productSale);
                    Helper.EF.Context.SaveChanges();
                }

                MessageBox.Show("Заказ создан успешно!", "Успех", MessageBoxButton.OK, MessageBoxImage.Information);

                MainWindow main = new MainWindow();
                main.Show();
                this.Close();
            }
        }

        private void BtnDown_Click(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;
            if (button == null)
            {
                return;
            }
            var product = button.DataContext as DB.VW_ProductList;

            switch (product.MeasureUnit)
            {
                case "единица товара":
                    if (product.Quantity > 1)
                    {
                        product.Quantity--;
                    }

                    SetListValues();
                    break;
                default:
                    if (product.Quantity > (decimal)0.1)
                    {
                        product.Quantity -= (decimal)0.1;
                    }

                    SetListValues();
                    break;
            }
        }

        private void BtnUp_Click(object sender, RoutedEventArgs e)
        {

            var button = sender as Button;
            if (button == null)
            {
                return;
            }
            var product = button.DataContext as DB.VW_ProductList;

            switch (product.MeasureUnit)
            {
                case "единица товара":
                    if (product.Quantity < 99)
                    {
                        product.Quantity++;
                    }

                    SetListValues();
                    break;
                default:
                    if (product.Quantity < 99)
                    {
                        product.Quantity += (decimal)0.1;
                    }

                    SetListValues();
                    break;
            }
        }

        private void BtnDeleteOrderProduct_Click(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;
            if (button == null)
            {
                return;
            }
            var product = button.DataContext as DB.VW_ProductList;

            Helper.OrderCart.orderCart.Remove(product);
            SetListValues();

            product.Quantity = 0;
        }

        private void CmbDeliveryType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DeliveryFee = (int)Helper.EF.Context.DeliveryType.ToList().Where(i => i == (CmbDeliveryType.SelectedItem as DB.DeliveryType)).FirstOrDefault().ExtraFee;
            SetListValues();
            TbDiscount.Text = DeliveryFee.ToString();
        }


        private void BtnConfirmPromo_Click(object sender, RoutedEventArgs e)
        {
            string code = TbDiscount.Text;
            
            if (!Helper.Discount.IsPromo(code) && !isPromoActivated)
            {
                MessageBox.Show("Данного промокода не существует", "Промоакция", MessageBoxButton.OK, MessageBoxImage.Stop);
            }

            else if (Helper.Discount.IsPromoUsed(code) && isPromoActivated) 
            {
                MessageBox.Show("Данный промокод уже использован ранее", "Промоакция", MessageBoxButton.OK, MessageBoxImage.Stop);
            }

            else
            {
                isPromoActivated = Helper.Discount.UsePromo(code);
                MessageBox.Show("Вы успешно использовали промокод!", "Промоакция", MessageBoxButton.OK, MessageBoxImage.Information);
            }

        }
    }
}
