﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BuilderApp.AppWindows
{
    /// <summary>
    /// Логика взаимодействия для AuthWindow.xaml
    /// </summary>
    public partial class AuthWindow : Window
    {
        DB.Staff authorizedEmployee = null;
        public AuthWindow()
        {
            InitializeComponent();
        }
        private void BtnEnter_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(TbPassword.Text) && !string.IsNullOrEmpty(TbLogin.Text))
            {
                authorizedEmployee = Helper.EF.Context.Staff.ToList().Where(i => i.Password == TbPassword.Text && i.Login == TbLogin.Text).FirstOrDefault();

                Helper.AuthorizedUser.SavedEmployee = authorizedEmployee;

                if (authorizedEmployee != null)
                {
                    MainWindow mainWindow = new MainWindow();
                    this.Close();
                    mainWindow.Show();

                }
                else
                {
                    MessageBox.Show("Данные пользователя неверны или не существуют", "Авторизация", MessageBoxButton.OK, MessageBoxImage.Stop);
                }
            }
        }
    }
}
