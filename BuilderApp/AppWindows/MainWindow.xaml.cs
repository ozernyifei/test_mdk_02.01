﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BuilderApp.AppWindows
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            //TbMainWindowHiName.Text = Helper.AuthorizedUser.SavedEmployee.LastName;
        }
        private void BtnProductList_Click(object sender, RoutedEventArgs e)
        {
            ProductWindow productWindow = new ProductWindow();
            productWindow.Show();
            this.Close();
        }

        private void BtnNewOrder_Click(object sender, RoutedEventArgs e)
        {
            OrderWindow orderWindow = new OrderWindow();
            orderWindow.Show();
            this.Close();
        }

        private void BtnExit_Click(object sender, RoutedEventArgs e)
        {
            AuthWindow authWindow = new AuthWindow();
            authWindow.Show();
            this.Close();

            Helper.AuthorizedUser.SavedEmployee = null;
        }
        private void BtnStaffList_Click(object sender, RoutedEventArgs e)
        {
            StaffWindow staffWindow = new StaffWindow();
            staffWindow.Show();
            this.Close();
        }
        private void BtnMagic_Click(object sender, RoutedEventArgs e)
        {
            
            MessageBox.Show("Добрая жена да жирные щи – другого добра не ищи.\nПро доброе дело говори смело. \nСделав добро, не кайся.", "Ты нажал волшебную кнопку", MessageBoxButton.OK, MessageBoxImage.Information);
            
        }
    }
}
