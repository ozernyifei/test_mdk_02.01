﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BuilderApp.AppWindows
{
    /// <summary>
    /// Логика взаимодействия для ProductListWindow.xaml
    /// </summary>
    public partial class ProductListWindow : Window
    {
        public ProductListWindow()
        {
            InitializeComponent();
            LVProductList.ItemsSource = Helper.EF.Context.VW_ProductList.ToList();
        }

        private void BtnError_Click(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;
            if (button == null)
            {
                return;
            }
            var product = button.DataContext as DB.VW_ProductList;


            if (product.AmountInStock >= 1)
            {
                Helper.OrderCart.orderCart.Add(product);
                switch (product.MeasureUnit)
                {
                    case "единица товара":
                        if (product.Quantity < 99)
                        {
                            product.Quantity++;
                        }
                        break;
                    default:
                        if (product.Quantity < 99)
                        {
                            product.Quantity += (decimal)0.1;
                        }
                        break;
                }
            }
            else
            {
                MessageBox.Show("Товар недоступен");
                return;
            }

            OrderWindow orderWindow = new OrderWindow();
            orderWindow.Show();
            this.Close();
        }

        private void BtnBack_Click(object sender, RoutedEventArgs e)
        {
            OrderWindow orderWindow = new OrderWindow();
            orderWindow.Show();
            this.Close();
        }
    }
}
