﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BuilderApp.AppWindows
{
    /// <summary>
    /// Логика взаимодействия для ProductWindow.xaml
    /// </summary>
    public partial class ProductWindow : Window
    {
        public ProductWindow()
        {
            InitializeComponent();
            SetListValues();
        }
        public void SetListValues()
        {
            LVProductList.ItemsSource = Helper.EF.Context.VW_ProductList.ToList();
        }

        private void LVProductList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var button = sender as Button;
            if (button == null)
            {
                return;
            }
            var service = button.DataContext as DB.VW_ProductList;

            Helper.OrderCart.orderCart.Add(service);
        }

        private void BtnBack_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mainMenu = new MainWindow();
            mainMenu.Show();
            this.Close();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ProductFilterWindow filterWindow = new ProductFilterWindow();
            this.Hide();
            filterWindow.ShowDialog();
            this.Show();
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            LVProductList.ItemsSource = Helper.EF.Context.VW_ProductList.ToList().FindAll(i => i.Title.ToLower().Contains(TbFiltr.Text.ToLower()));
        }
    }
}
