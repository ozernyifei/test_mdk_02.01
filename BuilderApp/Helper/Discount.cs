﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuilderApp.Helper
{
    public class Discount
    {
        public static Dictionary<string, bool> promocode = new Dictionary<string, bool>()
        {
            {"ABCDEF", false },
            {"BCDEFG", false },
            {"CDEFGH" , false },
            {"DEFGHI", false }
        };
        public static decimal SetDiscount(decimal discount, decimal startPrice)
        {   
            return startPrice - (startPrice * discount);
        }

        public static bool IsBlackFriday()
        {
            return DateTime.Now.DayOfYear >= 310 && DateTime.Now.DayOfYear <= 320;
        }

        public static bool IsPromo(string code)
        {
            return promocode.ContainsKey(code);
        }

        public static bool IsPromoUsed(string code)
        {
            return promocode[code] && promocode.ContainsKey(code);
        }

        public static bool UsePromo(string code) 
        { 
            
            if (promocode.ContainsKey(code) && !promocode[code])
            {
                promocode[code] = true;

            }
            return IsPromo(code) && promocode[code];

        }
    }
}
