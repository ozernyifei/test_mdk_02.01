# 3ИСП11-20 Лябин Александр

Техническое задание - [тут](/materials/ТЗ_Лябин.docx)

## ER - диаграмма
![](/materials/ER_Diagram.png)

## Use case
![](/materials/use_case.png)
## Физическая модель данных
![](/materials/DB_model.png)
## Окно авторизации
![](/materials/auth_window.png)

## Окно ошибки авторизации
![](/materials/error_auth.png)
    
## Основное окно
![](/materials/main_window.png)

## Каталог товаров
![](/materials/shop_window.png)

## Окно заказа
![](/materials/order_window.png)

## Результат заказа
![](/materials/result_order.png)    

## Диаграмма классов
![](/materials/class_diagram.png)
## Диаграмма деятельности
![](/materials/activity_diagram.png)
## Диаграмма последовательности
![](/materials/order_diagram.png)
